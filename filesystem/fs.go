//   Copyright 2017 Dustin Holman
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package filesystem

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/holman_dw/downloader"
)

// Manager manages the downloadable items in a file system
type Manager struct {
	root string // root directory

	mu        sync.RWMutex           // protects the fs map and available slice during discovery
	fs        map[string]os.FileInfo // [relpath]info
	available []string
}

// Get gets a Downloadable file
func (m *Manager) Get(relpath string) (downloader.Downloadable, error) {
	info, ok := m.fs[relpath]
	if !ok {
		return file{}, fmt.Errorf("unable to find file %s", relpath)
	}
	return file{path: filepath.Join(m.root, relpath), info: info}, nil
}

// NewManager creates a new filesystem Manager
func NewManager(root string) (*Manager, error) {
	m := &Manager{root: root}
	err := m.discover()
	return m, err
}

func (m *Manager) discover() error {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.fs = make(map[string]os.FileInfo)
	m.available = []string{}
	err := filepath.Walk(m.root, func(fp string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			relpath, err := filepath.Rel(m.root, fp)
			if err != nil {
				return err // panic? nah
			}
			m.fs[relpath] = info
			m.available = append(m.available, relpath)
		}
		return err
	})
	return err
}

// Available returns a slice of the available file names
func (m *Manager) Available() []string {
	if err := m.discover(); err != nil {
		return []string{}
	}
	m.mu.RLock()
	defer m.mu.RUnlock()
	return m.available
}

// file represents a downloadable file
type file struct {
	path string
	info os.FileInfo
}

// Reader todo document me
func (f file) ReadCloser() (io.ReadCloser, error) {
	return os.Open(f.path)
}

// Name todo document me
func (f file) Name() string {
	return f.info.Name()
}

// Size todo document me
func (f file) Size() int64 {
	return f.info.Size()
}
