//   Copyright 2017 Dustin Holman
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/holman_dw/downloader"
	fs "gitlab.com/holman_dw/downloader/filesystem"
)

var manager downloader.DownloadManager

// Response serves as a json response for requests to /available
type Response struct {
	Available []string `json:"available"`
}

// Available handles /available route
func Available(w http.ResponseWriter, r *http.Request) {
	av := manager.Available()
	resp := Response{Available: av}
	js, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, "error getting available files", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

// Download handles the `/download` route. The url query parameter "filename"
// is used to find the appropriate file.
func Download(w http.ResponseWriter, r *http.Request) {
	url := r.URL.Query()
	name, ok := url["filename"]
	if !ok {
		http.Error(w, "must provide `filename` parameter", http.StatusBadRequest)
		log.Printf("ip: %s attempted to download invalid file: %s", r.RemoteAddr, name)
		return
	}
	if len(name) != 1 || len(name[0]) < 1 {
		http.Error(w, "exactly one filename per request", http.StatusBadRequest)
		log.Printf("ip: %s invalid request", r.RemoteAddr)
		return
	}
	dl, err := manager.Get(name[0])
	if err != nil {
		http.Error(w, fmt.Sprintf("file `%s` not found", name), http.StatusBadRequest)
		log.Printf("ip: %s attempted to download invalid file: %s", r.RemoteAddr, name)
		return
	}
	if err := send(dl, w); err != nil {
		log.Printf("error sending file: %v", err)
		http.Error(w, fmt.Sprintf("500 - error sending file"), http.StatusInternalServerError)
		return
	}
}

func send(dl downloader.Downloadable, w http.ResponseWriter) error {
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", dl.Name()))
	w.Header().Set("Content-Length", fmt.Sprintf("%d", dl.Size()))
	readCloser, err := dl.ReadCloser()
	if err != nil {
		return err
	}
	defer readCloser.Close()
	if _, err := io.Copy(w, readCloser); err != nil {
		return err
	}
	return nil
}

func usage() {
	fmt.Println("\nusage: server root\n\troot: Root directory from which files will be served. All files in subdirectories will be served.")
}

func main() {
	// temporary location for files to serve
	args := os.Args
	if len(args) != 2 {
		usage()
		os.Exit(2)
	}
	root := args[1]
	f, err := filepath.Abs(root)
	if err != nil {
		log.Fatal(err)
	}
	info, err := os.Stat(root)
	if err != nil {
		log.Printf("error, os.Stat(\"%s\"): %v", root, err)
		usage()
		os.Exit(2)
	}
	if !info.IsDir() {
		log.Printf("\"%s\" is a file. Root must be a directory", root)
		usage()
		os.Exit(2)
	}

	// currently, manager is a file system (fs) manager
	manager, err = fs.NewManager(f)
	if err != nil {
		log.Fatalf("error starting manager: %v", err)
	}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) { fmt.Fprintf(w, htmlData) })
	http.HandleFunc("/download", Download)
	http.HandleFunc("/available", Available)
	log.Fatal(http.ListenAndServe(":8000", nil))
}

var htmlData = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>File Downloads</title>
</head>
<body>
    <h2>Stuff to Download!</h2>
    <div id="stuff"></div>    
</body>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">
    </script>
    <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
        integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
        crossorigin="anonymous">

    <script>
        $(document).ready(() => {
            $.get(
                "/available",
                {},
                (data) => {
                    console.log(data)
                    let htmlData = "<ul>\n"
                    let av = data.available
                    for (let i = 0; i < av.length; i++ ) {
                        let name = av[i]
                        htmlData += "<li><a href='/download?filename="+name+"'>"+name+"</a></li>\n"
                    }
                    htmlData += "</ul>"
                    $("#stuff").html(htmlData)
                }
            )
        })

    </script>
</html>`
