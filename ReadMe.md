# downloader

This project is an attempt to write an extensible server to serve files
on a web server. The docs are available [here](https://godoc.org/gitlab.com/holman_dw/downloader).

For the default file server:

```
cd server/
go run server.go /servedata/
```

Go to `http://localhost:8000/` to see a list of downloadable files. There are clickable links to
download files. The endpoint `/available` provides a json array of available filepaths.
These paths can be directly accessed by URL.

```
# I have a picture of my black, fat cat Binx in `pictures/` called `binx.jpg`.
curl "localhost:8000/download?filename=pictures/binx.jpg" > binxTheCat.jpg
```

# About

The DownloadManager maintains a current list of relative file paths
available for download in the supplied root directory. This list is recursive
and includes all files in subdirectories.


# TODO
* Make `server/server.go` more easily use other data formats
* Implement `Downloadable` and `DownloadManager` interfaces for AWS S3, Google Cloud Storage, etc.
* Add more API documentation
* Potential feature: add zip/tarball download if directory is requested.
* This needs tests, of course.

# Contact
Hit me up on twitter [@holman_dw](https://twitter.com/holman_dw)

# Contributing
If you'd like to contribute, reach out to me and we can figure out what
needs to be done that I'm not already working on.