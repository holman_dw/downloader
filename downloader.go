//   Copyright 2017 Dustin Holman
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

// Package downloader holds the interfaces for the download server in
// server/server.go.
package downloader

import (
	"io"
)

// Downloadable is the interface for copying a file
// from some storage mechanism to a web client.
type Downloadable interface {
	ReadCloser() (io.ReadCloser, error)
	Name() string
	Size() int64
}

// DownloadManager represents the interface
// for serving a group of `Downloadable` files.
type DownloadManager interface {
	Get(name string) (Downloadable, error)
	Available() []string
}
