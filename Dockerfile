FROM golang

ADD . /go/src/gitlab.com/holman_dw/downloader/

RUN go install gitlab.com/holman_dw/downloader/server/

EXPOSE 8000

ENTRYPOINT /go/bin/server /go/src/gitlab.com/holman_dw
